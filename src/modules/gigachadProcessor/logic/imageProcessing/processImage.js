import {eventBus} from "../eventBus/eventBus";
import {eventBusEvent} from "../eventBus/eventBusEvent";
import {detect} from "./api/detect";

export const processImage = (image, webView) => {
  return new Promise(async (resolve) => {
    eventBus.once(eventBusEvent.onImageProcessed, (data) => {
      resolve(data);
    });

    const result = await detect(image);

    // this done because of:
    // * react native cant draw on images out of the box
    // * freezing on every image sending to server
    // * there was no other way to fix this
    webView.injectJavaScript(`
      function dataURLtoFile(dataurl, filename) {
          var arr = dataurl.split(','),
              mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[arr.length - 1]), 
              n = bstr.length, 
              u8arr = new Uint8Array(n);
          while(n--){
              u8arr[n] = bstr.charCodeAt(n);
          }
          return new File([u8arr], filename, {type:mime});
      }

      async function detectWeed(image) {
          const headers = {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
          };
        
        const file = await fetch(image)
          .then(res => res.blob())
          .then(blob => {
            return new File([blob], "File name",{ type: "image/jpg" })
          })
        const formData = new FormData();
        formData.append("image", file, 'image.jpg');
        
        console.log(formData);
      
        const requestOptions = {
          method: 'POST',
          body: formData,
          redirect: 'follow'
        };
        
        console.log(data, 'here2');
      
        return fetch("https://gigachad.surdogram.ru/api/detect_weed", requestOptions)
          .then(response => {
            return response.json()
          })
          .then((data) => {
            console.log(data, 'here1');
                   
            return [...data.boxes.map((array) => {
              return {
                x: array[0],
                y: array[1],
                width: array[2],
                height: array[3],
                group: array[4]
              }
            })];
          })
          .catch((err) => {
            console.log(JSON.stringify(err), 'here123');
          });
      }
      
      function filterPhoto(image, result) {
        return new Promise((resolve, reject) => {
          const canvas = document.createElement("canvas");
          const ctx = canvas.getContext('2d');
          const targetImage = new Image();
      
          targetImage.onload = () => {
            canvas.width = targetImage.width;
            canvas.height = targetImage.height;
            ctx.drawImage(targetImage, 0, 0);
                        
            ctx.lineWidth = 10;
            ctx.strokeStyle = "#1A91FF";
            
            for (const { x, y, width, height, group } of result) {
                ctx.strokeRect(x, y, width, height);
                ctx.font = "bold 100px Arial";
                ctx.fillStyle = "#1A91FF";
                ctx.fillText(group, x, y - 50);
            }
            
            const dataUrl = canvas.toDataURL("base64");
            resolve(dataUrl);
          };
          targetImage.src = image;
        });
      }
      
      async function main() {        
        filterPhoto("${image}", ${JSON.stringify(result)})
          .then((result) => window.ReactNativeWebView.postMessage(result))
          .catch((error) => window.ReactNativeWebView.postMessage(''))
      };
      
      main();
    `);
  });
}